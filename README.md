# popnet_final

## Requirements
To get results, the following data are needed in the following folders :
In a folder Project_Data, create the following folders.
* Ancillary_data : eu_lakes, CLC databases for years 1990 and 2015 in a corine folder, train stations shapefile in european_train_stations folder, groads shapefile in groads_europe folder, lakes shapefile in lakes folder and slope tiff in slope folder.
* GADM : gadm shapefile data (layer 0 and 2)
* GHS : GHS data for years 1975, 1990, 2000 and 2015

## Files to change
The volumes must be changed in docker-compose.yml.

/usr/src/app/data must be mounted to the folder created to contain the created data.
/usr/src/app/Project_data must be mounted to the Project_data folder.

## How to use

### Data preparation :
Steps to run the program :
* Build image docker popnet_db_base from the Dockerfile in Docker_DB_Base
* Use docker-compose up to build program

### Machine Learning
* Mount /var/popnet_machinelearning/configs to the configs folders
* Mount /var/popnet_machinelearning/data to the data
* Mount the results folder.
