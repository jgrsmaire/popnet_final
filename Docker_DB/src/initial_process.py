# Imports
import subprocess
import os
import gdal
import ogr
import osr
import psycopg2
import time
from postgres_queries import run_queries
from rast_to_vec_grid import rasttovecgrid
from postgres_to_raster import psqltoshp
from postgres_to_raster import shptoraster
from import_to_postgres import import_to_postgres
import shutil
from process_by_polygon import process
from island_detection import multipoly2poly,addPolygon,separating_islands


def init_process(country, pghost, pgport, pguser, pgpassword, pgdatabase, ancillary_data_folder_path,
                 gadm_folder_path, ghs_folder_path, temp_folder_path, merge_folder_path, finished_data_path,python_scripts_folder_path):
    # Create temp folder, merge folder and finished_data folder if they don't exist 
        # create temp folder
        if not os.path.exists(temp_folder_path):
            os.makedirs(temp_folder_path)
        # create merge folder
        if not os.path.exists(merge_folder_path):
            os.makedirs(merge_folder_path)
        # create finished_data folder
        if not os.path.exists(finished_data_path):
            os.makedirs(finished_data_path)
        # create country folder within the finished_data folder
        country_path = finished_data_path + "/{0}".format(country.title())
        if not os.path.exists(country_path):
            os.makedirs(country_path)
        
        print("Starting process for {0}".format(country.title()))
        print("Extracting {0} from GADM data layer".format(country.title()))
        # select country in GADM and write to new file
        input_gadm_dataset = gadm_folder_path + "/gadm28_adm0.shp"
        output_country_shp = temp_folder_path + "/GADM_{0}.shp".format(country)
        country_name = country.replace("_", " ")
        country_name = country_name.title()
        sql_statement = "NAME_0='{0}'".format(country_name)
        country_shp = 'ogr2ogr -where "{0}" {1} {2} -lco ENCODING=UTF-8'.format(sql_statement, output_country_shp, input_gadm_dataset)
        subprocess.call(country_shp, shell=True)
        
        print("Making each polygon a feature")
        gdal.UseExceptions()
        driver = ogr.GetDriverByName('ESRI Shapefile')
        in_ds = driver.Open(output_country_shp, 0)
        in_lyr = in_ds.GetLayer()
        outputshp = temp_folder_path+'/GADM_{0}_multi.shp'.format(country)
        if os.path.exists(outputshp): 
            driver.DeleteDataSource(outputshp)  
        out_ds = driver.CreateDataSource(outputshp) 
        out_lyr = out_ds.CreateLayer("GADM", geom_type=ogr.wkbPolygon) 
        multipoly2poly(in_lyr, out_lyr)
        out_ds = None
        in_ds = None

        # create projection file for GADM multi
        driver = ogr.GetDriverByName('ESRI Shapefile')
        dataset = driver.Open(output_country_shp)
        layer = dataset.GetLayer()
        spatialRef = layer.GetSpatialRef()
        spatialRef.MorphToESRI()
        file = open(temp_folder_path + '/GADM_{0}_multi.prj'.format(country), 'w')
        file.write(spatialRef.ExportToWkt())
        file.close()      

        #Creating folder to put every partial GADM in
        gadm_path_country = "{1}/GADM_{0}".format(country,temp_folder_path)
        if not os.path.exists(gadm_path_country):
            os.makedirs(gadm_path_country) 

        
        
        #Creating the different shapefiles and retrieve the number of shapefiles
        infile = temp_folder_path+'/GADM_{0}_multi.shp'.format(country)
        n = separating_islands(gadm_path_country,infile,country)

        #Making every operation depending only on country name
        # Extracting train stations for the chosen country
        print("--- Creating train stations for {0} ---".format(country))
        infile = ancillary_data_folder_path + "/european_train_stations/european-train-stations.shp"
        outfile = temp_folder_path + "/european_train_stations.shp"
        country_code_dict = {'andora':'AD','albania':'AL','austria':'AT','bosnia':'BA','belgium':'BE','bulgaria':'BG','belarus':'BY','switzerland':'CH','czech_republic':'CZ','germany':'DE','denmark':'DK','estonia':'EE','spain':'ES','finland':'FI','France':'FR','united_kingdom':'GB','greece':'GR','croatia':'HR','hungary':'HU','ireland':'IE','italy':'IT','lithuania':'LT','luxembourg':'LU','latvia':'LV','moldova':'MD','montenegro':'ME','macedonia':'MK','malta':'MT','netherlands':'NL','norway':'NO','poland':'PL','portugal':'PT','romania':'RO','serbia':'RS','sweden':'SE','slovenia':'SI','slovakia':'SK','ukraine':'UA'}
        sql_statement = "country='{0}'".format(country_code_dict[country])
        cmds = 'ogr2ogr -where "{0}" {1} {2}'.format(sql_statement, outfile, infile)
        subprocess.call(cmds, shell=True)

        # Extracting municipalities from gadm for the chosen country
        print("--- Creating municipality layer for {0} ---".format(country))
        infile = gadm_folder_path + "/gadm28_adm2.shp"
        outfile = temp_folder_path + "/municipal.shp".format(country)
        sql_statement = "NAME_0='{0}'".format(country)
        cmds = 'ogr2ogr -where "{0}" {1} {2}'.format(sql_statement, outfile, infile)
        subprocess.call(cmds, shell=True)
        

        for i in range(n): #For each part of the country
            start_part_timer = time.time()
            print("-- PROCESSING PART {0} OF {1} --".format(i+1,n))
            countryi  = "{0}_{1}".format(country,i)

            country_path = finished_data_path + "/{0}".format(countryi)
            if not os.path.exists(country_path):
                    os.makedirs(country_path)

            #Creating folder for tiff to merge       
            countryi_merge_path = merge_folder_path + "/{0}".format(countryi)
            if not os.path.exists(countryi_merge_path):
                os.makedirs(countryi_merge_path)
                
            output_country_shp = "{1}/GADM_{0}/gadm_{0}_{2}.shp".format(country,temp_folder_path,i)

            # Create bounding box around the chosen part
            #Get a Layer's Extent
            inShapefile = output_country_shp
            inDriver = ogr.GetDriverByName("ESRI Shapefile")
            inDataSource = inDriver.Open(inShapefile, 0)
            inLayer = inDataSource.GetLayer()
            extent = inLayer.GetExtent()

            # Create a Polygon from the extent tuple
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(extent[0], extent[2])
            ring.AddPoint(extent[1], extent[2])
            ring.AddPoint(extent[1], extent[3])
            ring.AddPoint(extent[0], extent[3])
            ring.AddPoint(extent[0], extent[2])
            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)

            # Save extent to a new Shapefile
            outShapefile = temp_folder_path + "/extent_{0}.shp".format(countryi)
            outDriver = ogr.GetDriverByName("ESRI Shapefile")

            # Remove output shapefile if it already exists
            if os.path.exists(outShapefile):
                outDriver.DeleteDataSource(outShapefile)
                
            # Create the output shapefile
            outDataSource = outDriver.CreateDataSource(outShapefile)
            outLayer = outDataSource.CreateLayer("extent_{0}".format(country), geom_type=ogr.wkbPolygon)

            # Add an ID field
            idField = ogr.FieldDefn("id", ogr.OFTInteger)
            outLayer.CreateField(idField)

            # Create the feature and set values
            featureDefn = outLayer.GetLayerDefn()
            feature = ogr.Feature(featureDefn)
            feature.SetGeometry(poly)
            feature.SetField("id", 1)
            outLayer.CreateFeature(feature)
            feature = None

            # Save and close DataSource
            inDataSource = None
            outDataSource = None

            # Create projection file for extent
            driver  = ogr.GetDriverByName('ESRI Shapefile')
            dataset = driver.Open(output_country_shp)
            layer   = dataset.GetLayer()
            spatialRef = layer.GetSpatialRef()
            spatialRef.MorphToESRI()
            file = open(temp_folder_path + '/extent_{0}.prj'.format(countryi), 'w')
            file.write(spatialRef.ExportToWkt())
            file.close()

            print("----- Creating train layer for {0} -----".format(countryi))
            infile   = temp_folder_path + "/european_train_stations.shp"
            cut_file = gadm_path_country + "/gadm_{0}.shp".format(countryi)
            outfile  = temp_folder_path + "/{0}_trains.shp".format(countryi)
            cmds     = 'ogr2ogr -spat {0} {1} {2} {3} -clipsrc {4} {5} {6}'.format(extent[0],extent[2],extent[1],extent[3],cut_file, outfile, infile)
            subprocess.call(cmds, shell=True)

            print("----- Creating train layer for {0} -----".format(countryi))
            infile   = temp_folder_path + "/municipal.shp".format(country)
            cut_file = gadm_path_country + "/gadm_{0}.shp".format(countryi)
            outfile  = temp_folder_path + "/{0}_municipal.shp".format(countryi)
            cmds     = 'ogr2ogr -spat {0} {1} {2} {3} -clipsrc {4} {5} {6}'.format(extent[0],extent[2],extent[1],extent[3],cut_file, outfile, infile)
            subprocess.call(cmds, shell=True) 

            process(temp_folder_path,ancillary_data_folder_path,country,countryi,countryi_merge_path,ghs_folder_path,gadm_path_country,python_scripts_folder_path,extent)

            time0 = time.time() - start_part_timer
            print("Processing {1}th parth took {0} s".format(time0,i))
