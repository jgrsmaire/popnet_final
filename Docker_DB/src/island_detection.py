# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 13:25:31 2019

@author: Julie
"""

import os 
from osgeo import ogr
from osgeo import gdal
import geopandas as gp
from shapely.geometry import Point
from shapely.geometry import Polygon
import shutil

def multipoly2poly(in_lyr, out_lyr):
    """
    """
    for in_feat in in_lyr: 
        geom = in_feat.GetGeometryRef() 
        if geom.GetGeometryName() == 'MULTIPOLYGON': 
            for geom_part in geom: 
                addPolygon(geom_part.ExportToWkb(), out_lyr) 
        else: 
            addPolygon(geom.ExportToWkb(), out_lyr) 

def addPolygon(simplePolygon, out_lyr):
    """
    """
    featureDefn = out_lyr.GetLayerDefn() 
    polygon = ogr.CreateGeometryFromWkb(simplePolygon) 
    out_feat = ogr.Feature(featureDefn) 
    out_feat.SetGeometry(polygon) 
    out_lyr.CreateFeature(out_feat) 
    

 
def separating_islands(temp_folder_path,gadm_multi_path,country,one_island=0):
    """
    Create different shapefile containing close entities of a same country
    :param temp_folder_path : path to the temporary folder
    :type temp_folder_path : string
    :param gadm_multi_path : path to the gadm shapefile with a feature for each polygon
    :type gadm_multi_path : string
    :param country : country
    :type country : string
    :param one_island : 0 or 1, separating island or not ?
    :type one_island : int
    :return : i number of shapefile created
    :rtype : int
    """
    i=0 #Number of shapefiles

    if one_island==0 : #Separating the country in multiple parts
        print("Separating islands ...")
        gadm = gp.read_file(gadm_multi_path)
        buffer = 0.5  #Buffer applied
    
        gadm["area_geom"] = gadm["geometry"].area #Get area_geometry column
        n = len(gadm.index) #Number of features
        gadm["idx"] = range(1,n+1) #Creating an id column

        while n !=0: #While there is geometries in gadm
            aire_max = gadm["area_geom"].max() #Get biggest 'island'
            part_max = gadm[gadm.area_geom == aire_max] #Get element associated with biggest 'island'

            extent = part_max.total_bounds #Calculate extend

            #Creating the extent as a shapely.polygon
            xmin = extent[0] - buffer
            xmax = extent[2] + buffer
            ymin = extent[1] - buffer
            ymax = extent[3] + buffer
            p1 = Point(xmin,ymax)
            p2 = Point(xmax,ymax)
            p3 = Point(xmax,ymin)
            p4 = Point(xmin,ymin)

            p1_coords = (p1.coords.xy[0][0],p1.coords.xy[1][0])
            p2_coords = (p2.coords.xy[0][0],p2.coords.xy[1][0])
            p3_coords = (p3.coords.xy[0][0],p3.coords.xy[1][0])
            p4_coords = (p4.coords.xy[0][0],p4.coords.xy[1][0])
    
            bb = Polygon([p1_coords,p2_coords,p3_coords,p4_coords])
            #Turn the extent into a geodataframe
            df = gp.GeoDataFrame(gp.GeoSeries(bb),columns=['geometry'])

            #Spatial join : keep every island within the extent(+buffer)
            df = gp.sjoin(gadm,df, how='inner',op='within')

            #Get everyelement in the spatial join
            objts = df["idx"].tolist()
            m = len(objts)
            print("GADM {0} is composed of {1} parts.".format(i,m))

            for el in objts : #Remove everyelement in the spatial join from gadm
                gadm = gadm[gadm["idx"] != el]

            #Transforming the multi-features into one feature
            df = df.dissolve(by='index_right')
        
            name = "{2}/gadm_{0}_{1}.shp".format(country,i,temp_folder_path) 
            df.to_file(driver='ESRI Shapefile',filename=name) #Creating the GADM shapefile
    
            i=i+1 
            n = len(gadm.index) #Remaining element in gadm
            
    else : #Not separating country in parts
        print("Not separating island")
        src_file = temp_folder_path + "/GADM_{0}.shp".format(country)
        dst_file = "{2}/gadm_{0}_{1}.shp".format(country,i,temp_folder_path)
        shutil.copy(src_file,dst_file)
    return(i)

        
    


