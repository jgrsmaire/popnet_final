# Imports
import subprocess
import os
import gdal
import ogr
import osr

import psycopg2
import time
from rast_to_vec_grid import rasttovecgrid

import shutil


def process(temp_folder_path,ancillary_data_folder_path,country,countryi,merge_folder_path,ghs_folder_path,gadm_path_country,python_scripts_folder_path,extent):
    # Clipping lakes layer to country ----------------------------------------------------------------------------------
    print("------------------------------ Creating water layer for {0} ------------------------------".format(country))
    clip_poly = temp_folder_path + "/extent_{0}.shp".format(countryi)
    in_shp = ancillary_data_folder_path + "/eu_lakes.shp"
    out_shp = temp_folder_path + "/eu_lakes_{0}.shp".format(countryi)
    cmd_shp_clip = "ogr2ogr -clipsrc {0} {1} {2}".format(clip_poly, out_shp, in_shp)
    subprocess.call(cmd_shp_clip, shell=True)

    print("----- Creating road layer for {0} -----".format(country))
    infile = ancillary_data_folder_path + "/groads_europe/gROADS-v1-europe.shp"
    cut_file = gadm_path_country + "/gadm_{0}.shp".format(countryi)
    outfile = temp_folder_path + "/{0}_roads.shp".format(countryi)
    cmds = 'ogr2ogr -spat {0} {1} {2} {3} -clipsrc {4} {5} {6}'.format(extent[0],extent[2],extent[1],extent[3],cut_file, outfile, infile)
    subprocess.call(cmds, shell=True) 

    # Recalculating coordinate extent of bbox to match ghs pixels and clipping ghs raster layers -----------------------
    print("Extracting {0} from GHS raster layer".format(country))
    for subdir, dirs, files in os.walk(ghs_folder_path):
        for file in files:
            if file.endswith(".tif"):
                name = file.split(".tif")[0]
                ghs_file_path = os.path.join(subdir, file)
                out_file_path = merge_folder_path + "/{0}_{1}.tif".format(name, countryi)
                country_mask = gadm_path_country + "/gadm_{0}.shp".format(countryi)

                # open raster and get its georeferencing information
                dsr = gdal.Open(ghs_file_path, gdal.GA_ReadOnly)
                gt = dsr.GetGeoTransform()
                srr = osr.SpatialReference()
                srr.ImportFromWkt(dsr.GetProjection())

                # open vector data and get its spatial ref
                dsv = ogr.Open(country_mask)
                lyr = dsv.GetLayer(0)
                srv = lyr.GetSpatialRef()

                # make object that can transorm coordinates
                ctrans = osr.CoordinateTransformation(srv, srr)

                lyr.ResetReading()
                ft = lyr.GetNextFeature()
                while ft:
                    # read the geometry and transform it into the raster's SRS
                    geom = ft.GetGeometryRef()
                    geom.Transform(ctrans)
                    # get bounding box for the transformed feature
                    minx, maxx, miny, maxy = geom.GetEnvelope()

                    # compute the pixel-aligned bounding box (larger than the feature's bbox)
                    left = minx - (minx - gt[0]) % gt[1]
                    right = maxx + (gt[1] - ((maxx - gt[0]) % gt[1]))
                    bottom = miny + (gt[5] - ((miny - gt[3]) % gt[5]))
                    top = maxy - (maxy - gt[3]) % gt[5]

                    cmd_clip = 'gdalwarp -te {0} {1} {2} {3} -tr {4} {5} -cutline {6} -srcnodata -3.4028234663852886e+38 \
                                    -dstnodata 0 {7} {8}'.format(
                    str(left), str(bottom), str(right), str(top), str(abs(gt[1])), str(abs(gt[5])),
                    country_mask, ghs_file_path, out_file_path)
                    subprocess.call(cmd_clip, shell=True)

                    ft = lyr.GetNextFeature()
                ds = None

        # ----- Clipping slope, altering resolution to match ghs pop and recalculating slope values ------------------------
    print("------------------------------ PROCESSING SLOPE ------------------------------")
    print("Extracting slope for {0}".format(country))
    # Getting extent of ghs pop raster
    data = gdal.Open(merge_folder_path + "/GHS_POP_1975_{0}.tif".format(countryi))
    wkt = data.GetProjection()
    geoTransform = data.GetGeoTransform()
    minx = geoTransform[0]
    maxy = geoTransform[3]
    maxx = minx + geoTransform[1] * data.RasterXSize
    miny = maxy + geoTransform[5] * data.RasterYSize
    data = None

    print("Altering slope raster resolution to 250 meter")
    # Clipping slope and altering resolution
    cutlinefile = gadm_path_country + "/GADM_{0}.shp".format(countryi)
    srcfile = ancillary_data_folder_path +"/slope/slope_europe.tif"
    dstfile = temp_folder_path + "/slope_250_{0}.tif".format(countryi)
    cmds = 'gdalwarp -tr 250 250 -cutline {4} -crop_to_cutline -srcnodata 255 -dstnodata 0 {5} {6}'\
            .format(minx, miny, maxx, maxy, cutlinefile, srcfile, dstfile)
    subprocess.call(cmds, shell=True)

    print("Recalculating slope raster values")
    # Recalculate slope raster values of 0 - 250 to real slope value 0 to 90 degrees
    outfile = temp_folder_path + "/slope_{0}_finished_vers_int.tif".format(countryi)
    cmds = 'python {0}/gdal_calc.py -A {1} --outfile={2} --calc="numpy.arcsin((250-(A))/250)*180/numpy.pi" --NoDataValue=0'\
            .format(python_scripts_folder_path, dstfile, outfile)
    os.system(cmds)

    print("Reprojection")
    infile  = temp_folder_path + "/slope_{0}_finished_vers_int.tif".format(countryi)
    outfile = merge_folder_path + "/slope_{0}_finished_vers.tif".format(countryi)
    cmds = 'gdalwarp -t_srs EPSG:54009 {0} {1}'.format(infile,outfile)
    os.system(cmds)

    # Creating polygon grid that matches the population grid -----------------------------------------------------------
    print("------------------------------ Creating vector grid for {0} ------------------------------".format(country))
    outpath = temp_folder_path + "/{0}_2015vector.shp".format(countryi)
    rasttovecgrid(outpath, minx, maxx, miny, maxy, 250, 250)


    # Creating polygon grid with larger grid size, to split the smaller grid and iterate in postgis --------------------
    print("------------------------------ Creating larger iteration vector grid for {0} ------------------------------"
              .format(country))
    outpath = temp_folder_path + "/{0}_iteration_grid.shp".format(countryi)
    rasttovecgrid(outpath, minx, maxx, miny, maxy, 50000, 50000)
