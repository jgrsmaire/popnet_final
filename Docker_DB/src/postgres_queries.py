import psycopg2
import time
import os
import subprocess
def run_queries(landname, pgdatabase, pguser, pghost, pgpassword):
    country = landname.lower()

    python_script_dir = os.path.dirname(os.path.abspath(__file__))
    temp_folder_path = python_script_dir + "/data/Temp"

    #connect to postgres
    conn = psycopg2.connect(
    host='db',
    port=5432,
    database='raster_database',
    user='postgres',
    password='postgres'
    )
    cur = conn.cursor()

    #Reprojection of the geometries in 54009 
    print("---------- Reprojection tables ----------")
    #cur.execute('ALTER TABLE {0}_adm ALTER COLUMN geom TYPE geometry(MultiPolygon,54009) USING ST_Transform(ST_SetSRID(geom,4326), 54009);'.format(country))
##    cur.execute('ALTER TABLE {0}_train ALTER COLUMN geom TYPE geometry(Point,54009) USING ST_Transform(ST_SetSRID(geom,4326), 54009);'.format(country))
##    cur.execute('ALTER TABLE {0}_lakes ALTER COLUMN geom TYPE geometry(MultiPolygon,54009) USING ST_Transform(ST_SetSRID(geom,4326), 54009);'.format(country))
##    cur.execute('ALTER TABLE {0}_municipal ALTER COLUMN geom TYPE geometry(MultiPolygon,54009) USING ST_Transform(ST_SetSRID(geom,4326), 54009);'.format(country))
    #Naming the projection of the following geometry
    cur.execute("SELECT UpdateGeometrySRID('{0}_iteration_grid','geom',54009);".format(country))
    conn.commit()
    cur.execute("SELECT UpdateGeometrySRID('{0}_2015vector','geom',54009);".format(country))
    conn.commit()
    cur.execute("SELECT UpdateGeometrySRID('{0}_adm','geom',54009);".format(country))
    conn.commit()
    cur.execute("SELECT UpdateGeometrySRID('{0}_train','geom',54009);".format(country))
    conn.commit()
    cur.execute("SELECT UpdateGeometrySRID('{0}_lakes','geom',54009);".format(country))
    conn.commit()
    cur.execute("SELECT UpdateGeometrySRID('{0}_municipal','geom',54009);".format(country))
    conn.commit()
    

    # Queries ----------------------------------------------------------------------------------------------------------
##    cur.execute("DROP TABLE chunk_nr2".format(country))
##    conn.commit()
##    cur.execute("DROP TABLE subdivided_denmark_0_water;".format(country)) 
##    conn.commit()

##    cur.execute("Select * FROM denmark_0_lakes") #get every column in a table
##    colnames = [desc[0] for desc in cur.description]
##    print(colnames)
    
    # Creating necessary tables ----------------------------------------------------------------------------------------
    print("---------- Creating necessary tables, if they don't exist ----------")
    print("Checking {0} bounding box table".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_tables WHERE schemaname = 'public' AND tablename = '{0}_bbox');".format(country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} bounding box table from administrative areas".format(country))
        # bbox from administrative(+buffer):
        cur.execute("create table {0}_bbox as \
                    SELECT ST_Buffer(ST_SetSRID(ST_Extent(geom),54009) \
                    ,250,'endcap=square join=mitre') as geom FROM {0}_adm;".format(country))
        conn.commit()
    else:
        print("{0} bounding box table already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking {0} subdivided ocean table".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_tables WHERE schemaname = 'public' AND tablename = '{0}_subdivided_ocean');".format(country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} subdivided ocean table".format(country))
        # Ocean from administrative + bbox:
        cur.execute("Select ST_Subdivide(ST_Difference({0}_bbox.geom, {0}_adm.geom)) as geom \
                            into {0}_subdivided_ocean FROM {0}_bbox, {0}_adm;".format(country))
        conn.commit()
    else:
        print("{0} subdivided ocean table already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking {0} cover analysis table".format(country))
    cur.execute(
        "SELECT EXISTS (SELECT 1 FROM pg_tables WHERE schemaname = 'public' AND tablename = '{0}_cover_analysis');".format(
            country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} cover analysis table".format(country))
        # Watercover percentage:
        cur.execute("Create table {0}_cover_analysis as \
                            (SELECT * \
                            FROM {0}_2015vector);".format(country))  # 4.3 sec
        conn.commit()

        cur.execute("SELECT UpdateGeometrySRID('{0}_cover_analysis','geom',54009);".format(country))
        conn.commit()
    
    else:
        print("{0} cover analysis table already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking {0} subdivided waterbodies table".format(country))
    cur.execute(
        "SELECT EXISTS (SELECT 1 FROM pg_tables WHERE schemaname = 'public' AND tablename = '{0}_water');".format(
            country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} subdivided waterbodies table".format(country))
        # Creating waterbodies layer
        cur.execute("create table {0}_water as \
                            with a as ( \
                            select {0}_lakes.wplknm, ST_Intersection({0}_lakes.geom, {0}_adm.geom) as geom \
                            FROM {0}_lakes, {0}_adm \
                            where ST_Intersects({0}_lakes.geom, {0}_adm.geom)) \
                            select geom FROM {0}_subdivided_ocean \
                            UNION \
                            select ST_Subdivide(ST_Union(geom)) from a;".format(country))  # 3.32 min
        conn.commit()
    else:
        print("{0} subdivided waterbodies table already exists".format(country))

    #-------------------------------------------------------------------------------------------------------------------
    print("Checking {0} subdivided municipality table".format(country))
    cur.execute(
        "SELECT EXISTS (SELECT 1 FROM pg_tables WHERE schemaname = 'public' AND tablename = '{0}_subdivided_municipal');".format(
            country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} subdivided municipality table".format(country))
        # create subdivided municipal
        cur.execute(
            "CREATE TABLE {0}_subdivided_municipal AS SELECT gid_2, ST_Subdivide({0}_municipal.geom, 40) AS geom FROM {0}_municipal;".format(
                country))
        conn.commit()
        cur.execute("ALTER TABLE {0}_subdivided_municipal ADD id_muni SERIAL;".format(country))
        conn.commit()

    else:
        print("{0} subdivided municipality table already exists".format(country))


    # Adding necessary columns to country cover analysis table ---------------------------------------------------------
    print("---------- Adding necessary columns to {0}_cover_analysis table, if they don't exist ----------".format(country))

    print("Checking {0} cover analysis - water cover column".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 \
                FROM information_schema.columns \
                WHERE table_schema='public' AND table_name='{0}_cover_analysis' AND column_name='water_cover');".format(country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} cover analysis - water cover column".format(country))
        # Adding water cover column to cover analysis table
        cur.execute(
            "Alter table {0}_cover_analysis ADD column water_cover double precision default 0, add column id SERIAL PRIMARY KEY;".format(
                country))  # 11.3 sec
        conn.commit()
    else:
        print("{0} cover analysis - water cover column already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking {0} cover analysis - road distance column".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 \
                    FROM information_schema.columns \
                    WHERE table_schema='public' AND table_name='{0}_cover_analysis' AND column_name='rdist');".format(
        country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} cover analysis - road distance column".format(country))
        # Adding road distance column to cover analysis table
        cur.execute(
            "Alter table {0}_cover_analysis ADD column rdist double precision default 50000;".format(
                country))  # 14.8 sec
        conn.commit()
    else:
        print("{0} cover analysis - road distance column already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking {0} cover analysis - corine cover 1990 column".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 \
                        FROM information_schema.columns \
                        WHERE table_schema='public' AND table_name='{0}_cover_analysis' AND column_name='corine_cover90');".format(
        country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} cover analysis - corine cover 1990 column".format(country))
        # Adding water cover 1990 to country cover analysis table
        cur.execute(
            "Alter table {0}_cover_analysis ADD column corine_cover90 double precision default 0;".format(country))
        conn.commit()
    else:
        print("{0} cover analysis - corine cover 1990 column already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking {0} cover analysis - corine cover 2012 column".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 \
                            FROM information_schema.columns \
                            WHERE table_schema='public' AND table_name='{0}_cover_analysis' AND column_name='corine_cover');".format(
        country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} cover analysis - corine cover 2012 column".format(country))
        # Adding water cover 2012 to country cover analysis table
        cur.execute(
            "Alter table {0}_cover_analysis ADD column corine_cover double precision default 0;".format(country))
        conn.commit()
    else:
        print("{0} cover analysis - corine cover 2012 column already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking {0} cover analysis - train stations column".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 \
                                FROM information_schema.columns \
                                WHERE table_schema='public' AND table_name='{0}_cover_analysis' AND column_name='station');".format(
        country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} cover analysis - train stations column".format(country))
        # Adding train stations column to country cover analysis table
        cur.execute("Alter table {0}_cover_analysis ADD column station int default 0;".format(country))
        conn.commit()
    else:
        print("{0} cover analysis - train stations column already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking {0} cover analysis - municipality column".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 \
                                    FROM information_schema.columns \
                                    WHERE table_schema='public' AND table_name='{0}_cover_analysis' AND column_name='municipality');".format(
        country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating {0} cover analysis - municipality column".format(country))
        # Adding municipality column to country cover analysis table
        cur.execute("Alter table {0}_cover_analysis ADD column municipality int default 0;".format(country))
        conn.commit()
    else:
        print("{0} cover analysis - municipality column already exists".format(country))


    # Indexing necessary tables ----------------------------------------------------------------------------------------
    print("---------- Indexing necessary tables, if they don't exist ----------")

    print("Checking gist index on {0} bounding box table".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_class c JOIN pg_namespace n ON n.oid = c.relnamespace \
                WHERE c.relname = '{0}_bbox_gix' AND n.nspname = 'public');".format(country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating gist index on {0} bounding box table".format(country))
        # Creating index on administrative areas bounding box layer
        cur.execute("CREATE INDEX {0}_bbox_gix ON {0}_bbox USING GIST(geom);".format(country))  # 22 msec
        conn.commit()
    else:
        print("Gist index on {0} bounding box table already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking gist index on {0} water table".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_class c JOIN pg_namespace n ON n.oid = c.relnamespace \
                    WHERE c.relname = '{0}_water_gix' AND n.nspname = 'public');".format(country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating gist index on {0} water table".format(country))
        # Creating index on water layer
        cur.execute("CREATE INDEX {0}_water_gix ON {0}_water USING GIST (geom);".format(country))  # 32 msec
        conn.commit()
    else:
        print("Gist index on {0} water table already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking id index on {0} cover analysis table".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_class c JOIN pg_namespace n ON n.oid = c.relnamespace \
                        WHERE c.relname = '{0}_cover_analysis_id_index' AND n.nspname = 'public');".format(country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating id index on {0} cover analysis table".format(country))
        # Create index on country water cover id
        cur.execute("CREATE INDEX {0}_cover_analysis_id_index ON {0}_cover_analysis (id);".format(country))  # 4.8 sec
        conn.commit()
    else:
        print("Id index on {0} cover analysis table already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------
    print("Checking gist index on {0} cover analysis table".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_class c JOIN pg_namespace n ON n.oid = c.relnamespace \
                            WHERE c.relname = '{0}_cover_analysis_gix' AND n.nspname = 'public');".format(country))
    check = cur.fetchone()
    if check[0] == False:
        print("Creating gist index on {0} cover analysis table".format(country))
        # Creating index on water layer
        cur.execute("CREATE INDEX {0}_cover_analysis_gix ON {0}_cover_analysis USING GIST (geom);".format(country))
        conn.commit()
    else:
        print("Gist index on {0} cover analysis table already exists".format(country))
    #-------------------------------------------------------------------------------------------------------------------


    # getting id number of chunks within the iteration grid covering the country ---------------------------------------
    ids = []
    cur.execute("SELECT gid FROM {0}_iteration_grid;".format(country))
    chunk_id = cur.fetchall()

    # saving ids to list
    for id in chunk_id:
        ids.append(id[0])


    # Processing queries / running the cover analysis-----------------------------------------------------------------------------------------------
    print("-------------------- PROCESSING COVERAGE ANALYSIS: {0} consists of {1} big chunks --------------------".format(country, len(ids)))

    # Calculating water cover percentage -------------------------------------------------------------------------------

    # preparing water table by subdividing country water table
    print("Creating subdivided water table")
    print("Checking {0} bounding box table".format(country))
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_tables WHERE schemaname = 'public' AND tablename = 'subdivided_{0}_water');".format(country))
    check = cur.fetchone()
    if check[0] == True:
        cur.execute("DROP TABLE subdivided_{0}_water;".format(country)) 
        conn.commit()
    cur.execute(
        "CREATE TABLE subdivided_{0}_water AS (SELECT ST_Subdivide({0}_water.geom, 40) AS geom FROM {0}_water)".format(
            country))
    # create index on water
    cur.execute("CREATE INDEX subdivided_{0}_water_gix ON subdivided_{0}_water USING GIST (geom);".format(country))


    print("Creating table with roads within the country")
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_tables WHERE schemaname = 'public' AND tablename = '{0}_iterate_roads');".format(country))
    check = cur.fetchone()
    if check[0] == True:
        cur.execute("DROP TABLE {0}_iterate_roads;".format(country)) 
        conn.commit()
    #Creating table with roads within the country
    cur.execute("CREATE TABLE {0}_iterate_roads AS (SELECT {0}_groads.gid, ST_Transform(ST_SetSRID({0}_groads.geom, 4326), 54009) AS geom FROM {0}_groads, {0}_adm \
                WHERE ST_DWithin({0}_adm.geom, ST_Transform(ST_SetSRID({0}_groads.geom, 4326), 54009), 1));".format(country))  # 1.10 min
    # creating index on roads table
    cur.execute("CREATE INDEX {0}_iterate_roads_gix ON {0}_iterate_roads USING GIST (geom);".format(country))  # 21 ms
    conn.commit()


    # Transforming corine 1990 to srs 54009, subdividing geom and selecting everything that intersects with the country
    print("Creating subdivided corine 2012 layer")
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_tables WHERE schemaname = 'public' AND tablename = 'subdivided_{0}_corine');".format(country))
    check = cur.fetchone()
    if check[0] == True:
        cur.execute("DROP TABLE subdivided_{0}_corine;".format(country))
        conn.commit()
    cur.execute("CREATE TABLE subdivided_{0}_corine AS (SELECT {0}_corine.objectid, code_12, area_ha, \
                    ST_Subdivide(ST_Transform({0}_corine.geom, 54009), 30) AS geom FROM {0}_corine, {0}_adm \
                    WHERE ST_Intersects(ST_Transform({0}_corine.geom, 54009), {0}_adm.geom));".format(country))
    conn.commit()
    # Index subdivided corine 2012 table
    print("Creating index on subdivided corine 2012 layer")
    cur.execute("CREATE INDEX subdivided_{0}_corine_gix ON subdivided_{0}_corine USING GIST (geom);".format(country))  # 175 ms
    conn.commit()
    
    print("Creating subdivided corine 1990 layer")
    cur.execute("SELECT EXISTS (SELECT 1 FROM pg_tables WHERE schemaname = 'public' AND tablename = 'subdivided_{0}_corine90');".format(country))
    check = cur.fetchone()
    if check[0] == True:
        cur.execute("DROP TABLE subdivided_{0}_corine90;".format(country))
        conn.commit()
    cur.execute("CREATE TABLE subdivided_{0}_corine90 AS (SELECT {0}_corine90.objectid, code_90, area_ha, \
                    ST_Subdivide(ST_Transform({0}_corine90.geom, 54009), 30) AS geom FROM {0}_corine90, {0}_adm \
                    WHERE ST_Intersects(ST_Transform({0}_corine90.geom, 54009), {0}_adm.geom));".format(country))
    conn.commit()
    # Index subdivided corine table
    print("Creating index on subdivided corine 1990 layer")
    cur.execute("CREATE INDEX subdivided_{0}_corine90_gix ON subdivided_{0}_corine90 USING GIST (geom);".format(country))  # 175 ms
    conn.commit()

    #Processing chunks

    # iterating through chunks
    for chunk in ids:
        time0 = time.time()
        # check if chunk is pure ocean

        path = temp_folder_path + "/{0}_gaaadm_queries.shp".format(country)
        cmd = 'ogr2ogr -f "ESRI Shapefile" {0} PG:"host={1} user={2} dbname={3} password={4}" \
        -sql "SELECT * FROM {5}_adm"'.format(path, pghost, pguser, pgdatabase,
                                                                            pgpassword, country)
        subprocess.call(cmd, shell=True)

        path = temp_folder_path + "/{0}_it_queries.shp".format(country)
        cmd = 'ogr2ogr -f "ESRI Shapefile" {0} PG:"host={1} user={2} dbname={3} password={4}" \
        -sql "SELECT * FROM {5}_iteration_grid"'.format(path, pghost, pguser, pgdatabase,
                                                                            pgpassword, country)
        subprocess.call(cmd, shell=True)

    
        cur.execute("SELECT {0}_iteration_grid.gid \
                            FROM {0}_iteration_grid, {0}_adm \
                            WHERE ST_Intersects({0}_iteration_grid.geom, {0}_adm.geom) \
                            AND {0}_iteration_grid.gid = {1};".format(country, chunk))
        result_check = cur.rowcount

        if result_check == 0:
            print("Chunk number: {0} \ {1} is empty, setting water = 100 procent".format(chunk, len(ids)))
            # Setting the values of the whole chunk in country_cover_analysis - water_cover to 100 procent
            cur.execute("WITH a AS (SELECT {0}_cover_analysis.id, {0}_cover_analysis.geom \
                        FROM {0}_cover_analysis, {0}_iteration_grid \
                        WHERE {0}_iteration_grid.gid = {1} \
                        AND ST_Intersects({0}_cover_analysis.geom, {0}_iteration_grid.geom)) \
                        UPDATE {0}_cover_analysis SET water_cover = 100 FROM a WHERE a.id = {0}_cover_analysis.id;".format(
                country, chunk))


            
        else:
            print("Chunk number: {0} \ {1} is not empty, Processing water...".format(chunk, len(ids)))
            # start single chunk query time timer
            t0 = time.time()
            # select cells that is within each chunk and create a new table

            cur.execute("DROP TABLE IF EXISTS chunk_nr{0}".format(chunk))
            conn.commit()
            cur.execute("CREATE TABLE chunk_nr{1} AS (SELECT {0}_cover_analysis.id, {0}_cover_analysis.geom \
                            FROM {0}_cover_analysis, {0}_iteration_grid \
                            WHERE {0}_iteration_grid.gid = {1} \
                            AND ST_Intersects({0}_cover_analysis.geom, {0}_iteration_grid.geom));".format(country,
                                                                                                          chunk))  # 1.6 sec
            conn.commit()
            # create index on chunk
            cur.execute("CREATE INDEX chunk_nr{0}_gix ON chunk_nr{0} USING GIST (geom);".format(chunk))  # 464 msec
            conn.commit()
            

            

            # calculating water cover percentage
            cur.execute("WITH a AS (SELECT chunk_nr{1}.id, sum(ST_AREA(ST_INTERSECTION(chunk_nr{1}.geom, subdivided_{0}_water.geom))/62500*100) as water \
                            FROM chunk_nr{1}, subdivided_{0}_water WHERE ST_intersects(chunk_nr{1}.geom, subdivided_{0}_water.geom) \
                            GROUP BY id) \
                            UPDATE {0}_cover_analysis SET water_cover = water from a \
                            WHERE a.id = {0}_cover_analysis.id;".format(country, chunk))

            # drop chunk_nr table
            cur.execute("DROP TABLE chunk_nr{0};".format(chunk))  # 22 ms
            conn.commit()
            
#ROADS
            # Create table containing centroids of the original small grid within the land cover of the country
            cur.execute("CREATE TABLE chunk_nr{1} AS (SELECT id, ST_Centroid({0}_cover_analysis.geom) AS geom \
                        FROM {0}_cover_analysis, {0}_iteration_grid \
                        WHERE {0}_iteration_grid.gid = {1} \
                        AND ST_Intersects({0}_iteration_grid.geom, {0}_cover_analysis.geom));".format(country, chunk))  # 1.7 sec
            # check if chunk query above returns values or is empty
            result_check = cur.rowcount

            if result_check == 0:
                print("Chunk number: {0} \ {1} has no roads, moving to next chunk".format(chunk, len(ids)))
                conn.rollback()
            else:
                conn.commit()
                print("Chunk number: {0} \ {1} is not empty, Processing roads...".format(chunk, len(ids)))

                # Index chunk
                cur.execute("CREATE INDEX chunk_nr{0}_gix ON chunk_nr{0} USING GIST (geom);".format(chunk))  # 175 ms
                conn.commit()

                # Create table containing water_cover cell id and distance ALL
                cur.execute("WITH a AS (SELECT Distinct ON (chunk_nr{1}.id) chunk_nr{1}.id as id, \
                ST_Distance(chunk_nr{1}.geom, {0}_iterate_roads.geom) AS r_dist from {0}_iterate_roads, chunk_nr{1}, {0}_adm \
                WHERE st_DWithin(chunk_nr{1}.geom, {0}_iterate_roads.geom, 30000) order by chunk_nr{1}.id, r_dist asc) \
                UPDATE {0}_cover_analysis SET rdist = r_dist from a WHERE a.id = {0}_cover_analysis.id;".format(country, chunk))  # 4.1 sec
                conn.commit()

                # Drop chunk_nr table
                cur.execute("DROP TABLE chunk_nr{0};".format(chunk))  # 22 ms
                conn.commit()

#CORINE90
                # Check if chunk intersects with corine cover layer
            cur.execute("SELECT {0}_iteration_grid.gid \
                             FROM {0}_iteration_grid, subdivided_{0}_corine90 \
                             WHERE ST_Intersects({0}_iteration_grid.geom, subdivided_{0}_corine90.geom) \
                             AND {0}_iteration_grid.gid = {1};".format(country, chunk))
            result_check = cur.rowcount

            if result_check == 0:
                print("Chunk number: {0} \ {1} is empty (corine90), moving to next chunk".format(chunk, len(ids)))

            else:
                print("Chunk number: {0} \ {1} is not empty, Processing corine 1990 ...".format(chunk, len(ids)))

                # select cells that is within each chunk and create a new table
                cur.execute("CREATE TABLE chunk_nr{1} AS (SELECT {0}_cover_analysis.id, {0}_cover_analysis.geom \
                                 FROM {0}_cover_analysis, {0}_iteration_grid \
                                 WHERE {0}_iteration_grid.gid = {1} \
                                 AND ST_Intersects({0}_cover_analysis.geom, {0}_iteration_grid.geom));".format(country, chunk))  # 1.6 sec
                conn.commit()

                # Index chunk
                cur.execute("CREATE INDEX chunk_nr{0}_gix ON chunk_nr{0} USING GIST (geom);".format(chunk))  # 175 ms
                conn.commit()

                # calculating corine 1990 coverage
                cur.execute("WITH a AS (SELECT chunk_nr{1}.id, sum(ST_AREA(ST_INTERSECTION(chunk_nr{1}.geom, subdivided_{0}_corine90.geom))/62500*100) as corinecover \
                                FROM chunk_nr{1}, subdivided_{0}_corine90 WHERE ST_intersects(chunk_nr{1}.geom, subdivided_{0}_corine90.geom) \
                                GROUP BY id) \
                                UPDATE {0}_cover_analysis SET corine_cover90 = corinecover from a WHERE a.id = {0}_cover_analysis.id;".format(country, chunk))
                conn.commit()

                # Drop chunk_nr table
                cur.execute("DROP TABLE chunk_nr{0};".format(chunk))  # 22 ms
                conn.commit()


#CORINE12

            # Check if chunk intersects with corine cover layer
            cur.execute("SELECT {0}_iteration_grid.gid \
                             FROM {0}_iteration_grid, subdivided_{0}_corine \
                             WHERE ST_Intersects({0}_iteration_grid.geom, subdivided_{0}_corine.geom) \
                             AND {0}_iteration_grid.gid = {1};".format(country, chunk))
            result_check = cur.rowcount

            if result_check == 0:
                print("Chunk number: {0} \ {1} is empty (corine 2012), moving to next chunk".format(chunk, len(ids)))

            else:
                print("Chunk number: {0} \ {1} is not empty, Processing corine 2012...".format(chunk, len(ids)))

                # select cells that is within each chunk and create a new table 
                cur.execute("CREATE TABLE chunk_nr{1} AS (SELECT {0}_cover_analysis.id, {0}_cover_analysis.geom \
                                 FROM {0}_cover_analysis, {0}_iteration_grid \
                                 WHERE {0}_iteration_grid.gid = {1} \
                                 AND ST_Intersects({0}_cover_analysis.geom, {0}_iteration_grid.geom));".format(country, chunk))  # 1.6 sec
                conn.commit()

                # Index chunk
                cur.execute("CREATE INDEX chunk_nr{0}_gix ON chunk_nr{0} USING GIST (geom);".format(chunk))  # 175 ms
                conn.commit()

                # calculating corine 2012 coverage
                cur.execute("WITH a AS (SELECT chunk_nr{1}.id, sum(ST_AREA(ST_INTERSECTION(chunk_nr{1}.geom, subdivided_{0}_corine.geom))/62500*100) as corinecover \
                                FROM chunk_nr{1}, subdivided_{0}_corine WHERE ST_intersects(chunk_nr{1}.geom, subdivided_{0}_corine.geom) \
                                GROUP BY id) \
                                UPDATE {0}_cover_analysis SET corine_cover = corinecover from a WHERE a.id = {0}_cover_analysis.id;".format(country, chunk))
                conn.commit()

                # Drop chunk_nr table
                cur.execute("DROP TABLE chunk_nr{0};".format(chunk))  # 22 ms
                conn.commit()
#TRAINS
            # Create table containing centroids of the original small grid within the land cover of the country
            cur.execute("CREATE TABLE chunk_nr{1} AS (SELECT id, ST_Centroid({0}_cover_analysis.geom) AS geom \
                            FROM {0}_cover_analysis, {0}_iteration_grid \
                            WHERE {0}_iteration_grid.gid = {1} \
                            AND ST_Intersects({0}_iteration_grid.geom, {0}_cover_analysis.geom) \
                            AND {0}_cover_analysis.water_cover < 99.999);".format(country, chunk))  # 1.7 sec
            # check if chunk query above returns values or is empty
            result_check = cur.rowcount

            if result_check == 0:
                print("Chunk number: {0} \ {1} has no train, moving to next chunk".format(chunk, len(ids)))
                conn.rollback()
            else:
                conn.commit()
                print("Chunk number: {0} \ {1} is not empty, Processing trains...".format(chunk, len(ids)))

                # Index chunk
                cur.execute("CREATE INDEX chunk_nr{0}_gix ON chunk_nr{0} USING GIST (geom);".format(chunk))  # 175 ms
                conn.commit()

                # Counting number of train stations within x km distance
                cur.execute("with a as (select chunk_nr{1}.id, count(*) from {0}_train, chunk_nr{1} \
                where st_dwithin(chunk_nr{1}.geom, ST_SetSRID({0}_train.geom, 54009), 10000) \
                group by chunk_nr{1}.id) \
                update {0}_cover_analysis set station = a.count from a where a.id = {0}_cover_analysis.id;".format(country, chunk))  # 4.1 sec
                conn.commit()

                # Drop chunk_nr table
                cur.execute("DROP TABLE chunk_nr{0};".format(chunk))  # 22 ms
                conn.commit()

#MUNICIPALITIES
            # Check if chunk intersects with corine cover layer
            cur.execute("CREATE TABLE chunk_nr{1} AS (SELECT {0}_cover_analysis.id, ST_Centroid({0}_cover_analysis.geom) AS geom \
                                 FROM {0}_cover_analysis, {0}_iteration_grid \
                                 WHERE {0}_iteration_grid.gid = {1} \
                                 AND ST_Intersects({0}_iteration_grid.geom, {0}_cover_analysis.geom) \
                                 AND {0}_cover_analysis.water_cover < 99.999);".format(country, chunk))
            result_check = cur.rowcount

            if result_check == 0:
                print("Chunk number: {0} \ {1} is empty, moving to next chunk".format(chunk, len(ids)))
                conn.rollback()

            else:
                print("Chunk number: {0} \ {1} is not empty, Processing municipalities...".format(chunk, len(ids)))
                conn.commit()

                # Index chunk
                cur.execute("CREATE INDEX chunk_nr{0}_gix ON chunk_nr{0} USING GIST (geom);".format(chunk))  # 175 ms
                conn.commit()

                cur.execute("WITH a AS (SELECT id_muni, id FROM {0}_subdivided_municipal, chunk_nr{1} WHERE ST_Intersects(chunk_nr{1}.geom, {0}_subdivided_municipal.geom)) \
                            UPDATE {0}_cover_analysis SET municipality = a.id_muni \
                            FROM a \
                            WHERE a.id = {0}_cover_analysis.id;".format(country, chunk))
                conn.commit()

                # Drop chunk_nr table
                cur.execute("DROP TABLE chunk_nr{0};".format(chunk))  # 22 ms
                conn.commit()

        time_chunk = time.time() - time0
        print("Processing chunk took {0} s".format(time_chunk))




    #Droping tables
    # drop subdivided water table
    cur.execute("DROP TABLE subdivided_{0}_water;".format(country))  # 22 ms
    conn.commit()

    # Drop roads iteration table
    cur.execute("DROP TABLE {0}_iterate_roads;".format(country))
    conn.commit()

    # Drop subdivided country corine table
    cur.execute("DROP TABLE subdivided_{0}_corine90;".format(country))  # 22 ms
    conn.commit()

    # Drop subdivided country corine table
    cur.execute("DROP TABLE subdivided_{0}_corine;".format(country))  # 22 ms
    conn.commit()


 #-------------------------------------------------------------------------------------------------------------------

    # closing connection
    cur.close()
    conn.close()    
