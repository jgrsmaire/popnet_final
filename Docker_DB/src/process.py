# Imports
import subprocess
import os
import gdal
import ogr
import osr
import psycopg2
import time
from postgres_queries import run_queries
from rast_to_vec_grid import rasttovecgrid
from postgres_to_raster import psqltoshp
from postgres_to_raster import shptoraster
from import_to_postgres import import_to_postgres
import shutil
from process_by_polygon import process
from island_detection import multipoly2poly,addPolygon,separating_islands
from initial_process import init_process


def process_data(country, pghost, pgport, pguser, pgpassword, pgdatabase, ancillary_data_folder_path,
                 gadm_folder_path, ghs_folder_path, temp_folder_path, merge_folder_path, finished_data_path,
                 python_scripts_folder_path, gdal_rasterize_path, init_prep, init_import_to_postgres,
                 init_run_queries, init_export_data, init_merge_data):
    #Start total preparation time timer
    start_total_algorithm_timer = time.time()
    print("Starting the process ...")
    

    country = country.lower()
    country = country.replace(" ", "_")

    
    # Extracting country from GADM and creating bounding box -----------------------------------------------------------
    if init_prep == "yes":
        if os.path.exists(temp_folder_path):
            shutil.rmtree(temp_folder_path)
        if os.path.exists(merge_folder_path):
            shutil.rmtree(merge_folder_path)
        if os.path.exists(finished_data_path):
            shutil.rmtree(finished_data_path)
        init_process(country, pghost, pgport, pguser, pgpassword, pgdatabase, ancillary_data_folder_path,
                 gadm_folder_path, ghs_folder_path, temp_folder_path, merge_folder_path, finished_data_path,python_scripts_folder_path)
        
        time0 = time.time() - start_total_algorithm_timer
        print("Initial process took {0} s".format(time0))

        

    # Importing data to postgres--------------------------------------------------------------------------------------
    if init_import_to_postgres == "yes":
        print("------------------------------ IMPORTING DATA TO POSTGRES ------------------------------")
        # Adding postgis and srs 54009 to postgres if it doesn't exist -----------------------------------------------------
        # connect to postgres
        conn = psycopg2.connect(
            host='db',
            port=5432,
            database='raster_database',
            user='postgres',
            password='postgres'
            )
        cur = conn.cursor()

        cur.execute("DROP SCHEMA public CASCADE;CREATE SCHEMA public;")
        conn.commit()

        # check for and add postgis extension if not existing
        cur.execute("SELECT * FROM pg_available_extensions \
                    WHERE name LIKE 'postgis';")
        cur.execute("CREATE EXTENSION postgis;")

        # Check for and add srid 54009 if not existing
        cur.execute("SELECT * From spatial_ref_sys WHERE srid = 54009;")
        check_srid = cur.rowcount
        if check_srid == 0:
            print("Adding SRID 54009 to postgres")
            projs = '"World_Mollweide"'
            geogcs = '"GCS_WGS_1984"'
            datum = '"WGS_1984"'
            spheroid = '"WGS_1984"'
            primem = '"Greenwich"'
            unit_degree = '"Degree"'
            projection = '"Mollweide"'
            param_easting = '"False_Easting"'
            param_northing = '"False_Northing"'
            param_central = '"Central_Meridian"'
            unit_meter = '"Meter"'
            authority = '"ESPG","54009"'
            cur.execute("INSERT into spatial_ref_sys (srid, auth_name, auth_srid, proj4text, srtext) values \
                 ( 54009, 'ESRI', 54009, '+proj=moll +lon_0=0 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs ', \
                'PROJCS[{0},GEOGCS[{1},DATUM[{2},SPHEROID[{3},6378137,298.257223563]],\
                PRIMEM[{4},0],UNIT[{5},0.017453292519943295]],PROJECTION[{6}],PARAMETER[{7},0],\
                PARAMETER[{8},0],PARAMETER[{9},0],UNIT[{10},1],AUTHORITY[{11}]]');"
                        .format(projs, geogcs, datum, spheroid, primem, unit_degree, projection,
                                param_easting, param_northing, param_central, unit_meter, authority))
            conn.commit()
        # closing connection
        cur.close()
        conn.close()
        start = time.time()
        gadm_path_country = "{1}/GADM_{0}".format(country,temp_folder_path)
        i=0
        for subdir, dirs, files in os.walk(gadm_path_country):
            for file in files:
                if file.endswith(".shp") and not file.endswith("3035.shp") and not file.endswith("54009.shp"): #For every part
                    print("Importing data for part {0}".format(i+1))
                    countryi = country+"_{}".format(i)
                    import_to_postgres(country,countryi, pghost, pgport, pguser, pgpassword, pgdatabase, temp_folder_path, ancillary_data_folder_path)
                    i=i+1

        time0 = time.time() - start
        print("Running queries took {0} s".format(time0))
        
        
 

    # Running postgres queries -----------------------------------------------------------------------------------------
    if init_run_queries == "yes":
        print("------------------------------ RUNNING POSTGRES QUERIES ------------------------------")
        start = time.time()
        gadm_path_country = "{1}/GADM_{0}".format(country,temp_folder_path)
        i=0
        for subdir, dirs, files in os.walk(gadm_path_country):
            for file in files:
                if file.endswith(".shp") and not file.endswith("3035.shp") and not file.endswith("54009.shp"): #For every part
                    
                    print("Importing data for part {0}".format(i+1))
                    countryi = country+"_{}".format(i)
                    run_queries(countryi, pgdatabase, pguser, pghost, pgpassword)
                    i=i+1

        time0 = time.time() - start
        print("Running queries took {0} s".format(time0))


        
        

    # Export layers from postgres to shp -------------------------------------------------------------------------------
    if init_export_data == "yes":
        print("------------------------------ EXPORTING DATA FROM POSTGRES ------------------------------")
        start = time.time()
        gadm_path_country = "{1}/GADM_{0}".format(country,temp_folder_path)
        i=0
        for subdir, dirs, files in os.walk(gadm_path_country):
            for file in files:
                if file.endswith(".shp") and not file.endswith("3035.shp") and not file.endswith("54009.shp"): #For every part
                    print("Exporting data for part {0}".format(i+1))
                    countryi = country+"_{}".format(i)
                    psqltoshp(countryi, pghost, pguser, pgpassword, pgdatabase, temp_folder_path)
                    shptoraster(countryi, gdal_rasterize_path, 250, 250, temp_folder_path, merge_folder_path)
                    i=i+1
        time0 = time.time() - start
        print("Exporting and rasterizing data took {0} s".format(time0))

    # Merging all ghs files into one multiband raster ------------------------------------------------------------------
    if init_merge_data == "yes":
        print("------------------------------ CREATING MERGED TIFF FILES ------------------------------")
        
        start = time.time()
        gadm_path_country = "{1}/GADM_{0}".format(country,temp_folder_path)
        
        i=0
        
        for subdir, dirs, files in os.walk(gadm_path_country):
            for file in files:
                if file.endswith(".shp") and not file.endswith("3035.shp") and not file.endswith("54009.shp"): #For every part
                    countryi = country+"_{}".format(i)
                    m_path = merge_folder_path + "/{0}".format(countryi)
                    print("Merging files for part {0}".format(i+1))
                    country_path = finished_data_path + "/{0}".format(countryi)
                    # Merging files for 1975
                    print("Merging files for 1975")
                    
                    outfile = country_path + "/{0}.tif".format(1975)
                    original_tif_pop = m_path + "/GHS_POP_1975_{0}.tif".format(countryi)
                    water = m_path + "/{0}_water_cover.tif".format(countryi)
                    road_dist = m_path + "/{0}_roads.tif".format(countryi)
                    slope = m_path + "/slope_{0}_finished_vers.tif".format(countryi)
                    corine = m_path + "/{0}_corine1990.tif".format(countryi)
                    train = m_path + "/{0}_train_stations.tif".format(countryi)
                    municipal = m_path + "/{0}_municipality.tif".format(countryi)
                    cmd_tif_merge = "python {0}/gdal_merge.py -o {1} -separate {2} {3} {4} {5} {6} {7} {8} ".format(python_scripts_folder_path, outfile, original_tif_pop,water, road_dist, slope, corine, train, municipal)
                    os.system(cmd_tif_merge)

                    # Merging files for 1990
                    print("Merging files for 1990")
                    outfile = country_path + "/{0}.tif".format(1990)
                    original_tif_pop = m_path + "/GHS_POP_1990_{0}.tif".format(countryi)
                    water = m_path + "/{0}_water_cover.tif".format(countryi)
                    road_dist = m_path + "/{0}_roads.tif".format(countryi)
                    slope = m_path + "/slope_{0}_finished_vers.tif".format(countryi)
                    corine = m_path + "/{0}_corine1990.tif".format(countryi)
                    train = m_path + "/{0}_train_stations.tif".format(countryi)
                    municipal = m_path + "/{0}_municipality.tif".format(countryi)
                    cmd_tif_merge = "python {0}/gdal_merge.py -o {1} -separate {2} {3} {4} {5} {6} {7} {8}" .format(python_scripts_folder_path, outfile, original_tif_pop,water, road_dist, slope, corine, train, municipal)
                    os.system(cmd_tif_merge)

                    # Merging files for 2000
                    print("Merging files for 2000")
                    outfile = country_path + "/{0}.tif".format(2000)
                    original_tif_pop = m_path + "/GHS_POP_2000_{0}.tif".format(countryi)
                    water = m_path + "/{0}_water_cover.tif".format(countryi)
                    road_dist = m_path + "/{0}_roads.tif".format(countryi)
                    slope = m_path+ "/slope_{0}_finished_vers.tif".format(countryi)
                    corine = m_path + "/{0}_corine2012.tif".format(countryi)
                    train = m_path + "/{0}_train_stations.tif".format(countryi)
                    municipal = m_path + "/{0}_municipality.tif".format(countryi)
                    cmd_tif_merge = "python {0}/gdal_merge.py -o {1} -separate {2} {3} {4} {5} {6} {7} {8}".format(python_scripts_folder_path, outfile, original_tif_pop,water, road_dist, slope, corine, train, municipal)
                    os.system(cmd_tif_merge)

                    # Merging files for 2000
                    print("Merging files for 2015")
                    outfile = country_path + "/{0}.tif".format(2015)
                    original_tif_pop = m_path + "/GHS_POP_2015_{0}.tif".format(countryi)
                    water = m_path + "/{0}_water_cover.tif".format(countryi)
                    road_dist = m_path + "/{0}_roads.tif".format(countryi)
                    slope = m_path + "/slope_{0}_finished_vers.tif".format(countryi)
                    corine = m_path + "/{0}_corine2012.tif".format(countryi)
                    train = m_path  + "/{0}_train_stations.tif".format(countryi)
                    municipal = m_path  + "/{0}_municipality.tif".format(countryi)
                    cmd_tif_merge = "python {0}/gdal_merge.py -o {1} -separate {2} {3} {4} {5} {6} {7} {8}" .format(python_scripts_folder_path, outfile, original_tif_pop,water, road_dist, slope, corine, train, municipal)
                    os.system(cmd_tif_merge)
                    i=i+1
        time0 = time.time() - start
        print("Merging files took {0} s".format(time0))

    # stop total algorithm time timer ----------------------------------------------------------------------------------
    stop_total_algorithm_timer = time.time()
    # calculate total runtime
    total_time_elapsed = (stop_total_algorithm_timer - start_total_algorithm_timer)/60
    print("Total preparation time for {0} is {1} minutes".format(country, total_time_elapsed))
