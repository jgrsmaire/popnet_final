import json
from bunch import Bunch
import os


def get_config_from_json(json_file):
    """
    Get the config from a json file
    :param json_file:
    :return: config(namespace) or config(dictionary)
    """
    # parse the configurations from the config json file provided
    with open(json_file, 'r') as config_file:
        config_dict = json.load(config_file)

    # convert the dictionary to a namespace using bunch lib
    config = Bunch(config_dict)

    return config, config_dict


def process_config(jsonfile):
    config, _ = get_config_from_json(jsonfile)

    config.summary_dir     = "/var/popnet_machinelearning/experiments/{0}/{1}/summary".format(config.exp_name, config.sub_exp)
    config.checkpoint_dir  = "/var/popnet_machinelearning/experiments/{0}/{1}/checkpoint/".format(config.exp_name, config.sub_exp)
    config.output_dir      = "/var/popnet_machinelearning/experiments/{0}/{1}/outputs/".format(config.exp_name, config.sub_exp)
    config.output_pred_dir = "/var/popnet_machinelearning/experiments/{0}/{1}/outputs/predictions".format(config.exp_name, config.sub_exp)
    config.output_dif_dir  = "/var/popnet_machinelearning/experiments/{0}/{1}/outputs/difference".format(config.exp_name, config.sub_exp)
    config.output_eval_dir = "/var/popnet_machinelearning/experiments/{0}/{1}/outputs/evaluation".format(config.exp_name, config.sub_exp)
    config.output_bbox_dir = "/var/popnet_machinelearning/experiments/{0}/{1}/outputs/bbox".format(config.exp_name, config.sub_exp)
    config.input_dir       = "/var/popnet_machinelearning/experiments/{0}/{1}/outputs/inputs/".format(config.exp_name, config.sub_exp)

    return config
