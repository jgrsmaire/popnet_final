import sys

import tensorflow as tf
import os
from osgeo import gdal
import osr
import numpy as np

sys.path.append('/var/popnet_machinelearning/')

from data_loader.data_generator import DataGenerator, PrepData, PrepTrainTest
from data_loader.data_loader import DataLoader
from models.pop_model import PopModel
from trainers.pop_trainer import PopTrainer
from utils.config import process_config
from utils.dirs import create_dirs
from utils.logger import Logger
from utils.utils import get_args

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ['KMP_DUPLICATE_LIB_OK']='True'



base_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(base_dir)

config_dir = '/var/popnet_machinelearning/configs'

args = get_args()
if args.config != 'None':
    config = process_config(args.config) #Determining relative paths
else:
    config = process_config('/var/popnet_machinelearning/configs/config.json')

data_dir = '/var/popnet_machinelearning/data/{}'.format(config.exp_name)

def main():
    # capture the config path from the run arguments
    # then process the json configration file
    # try:


    data_loader = DataLoader(data_dir, config) #Configuration of the loader (OO)
    data_loader.load_directory('.tif')
    data_loader.create_np_arrays() 
    data_loader.create_data_label_pairs()

    preptt = PrepTrainTest(config, data_loader) #Implementation of the preparation of the training test

    for i in range(len(data_loader.data_label_pairs)): #for every pairs
        x_data = data_loader.data_label_pairs[i][0][:, :, :]
        y_true = data_loader.data_label_pairs[i][1][:, :, 0]
        preptt.add_data(x_data, y_true)

    # Create the experiments dirs
    create_dirs([config.summary_dir, config.checkpoint_dir, config.input_dir])

    # Create tensorflow session
    sess = tf.compat.v1.Session()

    # Create instance of the model you want
    model = PopModel(config)

    # Load model if exist
    model.load(sess)

    # Create Tensorboard logger
    logger = Logger(sess, config)
    logger.log_config()

    # Create your data generator
    data = DataGenerator(config, preptraintest = preptt)

    data.create_traintest_data()

    # Create trainer and path all previous components to it
    trainer = PopTrainer(sess, model, data, config, logger)

    # Train model
    trainer.train()

    # Test model



if __name__ == '__main__':
    tf.compat.v1.reset_default_graph()
    main()
